#!/usr/bin/env node
// import fs for fileIO
const fs = require('fs');

// a function which takes a filename and a callback to read a complete
// file into the buffer at one time
const read_file = (fname, cb) => {
    // get status of the file and define a file-open callback
    fs.stat(fname, (err, stats) => {
        if(err) {
            // if stat returns an error, null will be passed to original callback,
            // triggering an exception
            cb(null);
        } else {
            // if file is available, open it in read mode and pass a file-read callback
            fs.open(fname, 'r', (err, fd) => {
                if (err) {
                    // if file open fails, pass null to the original callback
                    cb(null);
                } else {
                    // if file opens, create an input buffer matching the size of the file
                    let inbuf = Buffer.alloc(stats.size);
                    // read the file into the buffer and pass an output callback 
                    fs.read(fd, inbuf, 0, stats.size, null, (err, bytesRead, buffer) => {
                        if(err) {
                            // if the read fails, pass null to the original callback
                            cb(null);
                        } else {
                            // Success!
                            // pass the buffer contents to the original callback
                            cb(buffer);
                        }
                    });
                }
            });
        }
    });
}

// open and read a file. Pass in an output callback
read_file('test.txt', (result) => {
    // the callback prints data from the file (or dies trying) 
    console.log(`Result is ${result.toString()}`);
});
