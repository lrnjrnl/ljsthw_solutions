#!/usr/bin/env node
// I'm testing "this" in different contexts
// In this particular test, I want to see what happens when
// a function is invoked with function invocation versus
// local invocation. I am also contrasting named function definition
// with the "function" keyword versus arrow functions.
//
// Let's start with a global arrow function
let a = () => {
    // what is the context?
    if (this === g || this === h || this === j || this === k) {
        console.log("a local arrow function");
    } else {
        console.log("a global arrow function");
    }
}

// And now a global function
function f () {
    // what is the context?
    if (this === g || this === h || this === j || this === k) {
        console.log("f local function");
    } else {
        console.log("f global function");
    }
}

// What happens when global functions are referenced from an object literal?
let g = {
    a : a,
    f : f,
    // Let's also try defining similar functions within the literal
    // arrow function within an object literal still has global "this"
    p : ()=>{ this===g ? console.log("p local arrow") : console.log("p global arrow");},
    // However function def within object literal has local "this"!
    q : function(){ this===g ? console.log("q local") : console.log("q global");}
}

H = function () {
    // creating references to the global functions in a constructor
    this.a = a;
    this.f = f;
    // defining similar functions within the constructor
    // arrow function in constructor has a local "this"
    this.r = ()=>{ this===h ? console.log("r local arrow") : console.log("r global")};
    // nested function def also has local "this"
    this.u = function() {this===h ? console.log("u local"):console.log("u global")};
}
h = new H();

// let's also see what happens if the function objects are passed as parameters
// to a constructor
J = function (func_a, func_f) {
    this.a = func_a;
    this.f = func_f;
}
j = new J(a, f);

// Finally, a and f are passed as callback functions to a method before invocation.
K = function () {
    this.n = function (func_a, func_f) {
        func_a();
        func_f();
    };
}
k = new K()

console.log("\nFunctions a and f are defined from global.");
console.log(">>> invoked as global function");
a(); // a global arrow function
f(); // f global function

console.log("\nFunctions p and q are defined within object literal.");
console.log(">>> invoked via object literal 'g'");
g.a(); // a global arrow function
g.f(); // f local function
g.p(); // p global arrow
g.q(); // q local

console.log("\nFunctions r and u are defined within constructor.");
console.log(">>> invoked via constructed object 'h'");
h.a(); // a global arrow function
h.f(); // f local function
h.r(); // r local arrow
h.u(); // u local

console.log("\n>>> a and f invoked via parameter to constructed object 'j'.");
j.a(); // a global arrow function
j.f(); // f local function

console.log("\n>>> a and f invoked as callbacks in method 'k.n()'.");
k.n(a, f);

// From these tests, it can be seen that arrow functions obey rules
// of lexical scope. That is, their scope is determined at the point of
// function definition. "this" refers to the context where the function
// is defined.
//
// I'm confused about the following:
// Regular global function definitions should obey dynamic scope rules
// if invoked as a callback function from a method. "this" refers to the context where the function is called.
// But in my tests, the regular function obeys static scope.
// What am I misunderstanding?