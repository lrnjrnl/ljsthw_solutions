#!/usr/bin/env node
const fs = require('fs').promises;

// you have to do nested calls any time you need the result of the previous
// calculuation
//

// this function doesn't get a callback
const read_file = (fname) => {
    // let the promises nesting hell begin
    // it looks as though promises is like conditionals nested in 
    // method arguments. "then" is the "if" blok, and "catch" is the "else"
    // block. The effect is similar to nesting try-except blocks in python.
    // Question: how does .then know if it's being passed a filehandle and
    // not an error?
    fs.open(fname, 'r').then((fh) => {
        // here's where open gets followed with 
        fh.stat().then((stat) => {
            let buf = Buffer.alloc(stat.size);

            fh.read(buf, 0, stat.size, null)
            .then((result) => {
                console.log(`Read ${result.bytesRead} bytes:${result.buffer.toString()}`);
            }).catch((err) => {
                console.log(`Error ${err}`);
            });
        }).catch((err) => console.log(err));
    }).catch((err) => console.log(err));
}

read_file('test.txt');
