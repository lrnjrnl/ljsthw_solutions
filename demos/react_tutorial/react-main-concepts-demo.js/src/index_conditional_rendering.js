import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.aaaaaaa
// Learn more about service workers: http://bit.ly/CRA

// this is button element to show when the user is not logged in.
let LoginButton = (props) => {
    return (
        <button onClick={props.onClick}>
            Login
        </button>
    );
}
// this is button element to show when the user is logged in.
let LogoutButton = (props) => {
    return (
        <button onClick={props.onClick}>
            Logout
        </button>
    );
}

let Greeting = (props) => {
    // login state from LoginControl is passed in via props
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
        return (
            <h1>Welcome, brother!</h1>
        )
    }
    return (
        <h1>Please log in</h1>
    );
}

class LoginControl extends React.Component {
    // instantiate the component, passing tag attributes in props object
    constructor(props) {
        // pass the tag attributes to the parent class via the props object   
        super(props);
        // create a state object to track login status
        this.state = {isLoggedIn: false};
    }
    // maintain lexical env of 'this' with arrow function for onClick callbacks
    handleLoginClick = () => {
        this.setState({isLoggedIn: true});
    }

    handleLogoutClick = () => {
        this.setState({isLoggedIn: false});
    }
    // conditional output
    render() {
        // This is unnecessary. It's only to keep conditions tidy.
        const isLoggedIn = this.state.isLoggedIn;
        // declare outside of if-else's block scope for button element to
        // persist after if-else block completes
        let button;
        // choose which version of the button to show
        if (isLoggedIn === true) {
            button = <LogoutButton onClick={this.handleLogoutClick} />
        } else if (isLoggedIn === false) {
            button = <LoginButton onClick={this.handleLoginClick} />
        }
        // send the greeting and button element back to the DOM renderer.
        return (
            <div>
                // call the greeting and pass it the login state
                <Greeting isLoggedIn={isLoggedIn} />
                // render the button element
                {button}
            </div>
        );
    }
}


ReactDOM.render(
    // instantiate and render the LoginControl component
    <LoginControl />,  
    document.getElementById('root')
);
