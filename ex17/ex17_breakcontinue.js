#!/usr/bin/env node
let count = 5;
label1:
while(count > 0) {
    console.log("in while", count);
    if(count > 2) {
        for(i = 0; i < 4; i++) {
            console.log("in for", i);
            if(i == 2) {
                count--;
                continue label1;
            }
        }
    } else if(count == 2) {
        count -= 2;
        console.log("continue in while");
        continue label1;
    } else {
        break;
    }
    count--;
}

label2:
console.log("Done.");
/* Anticipated outcome:
count = 5
>>> in while 5
i = 0
>>> in for 0
i = 1
>>> in for 1
i = 2
>>> in for 2
count = 4
>>> in while 4
i = 0
>>> in for 0
i = 1
>>> in for 1
i = 2
>>> in for 2
count = 3
>>> in while 3
i = 0
>>> in for 0
i = 1
>>> in for 1
i = 2
>>> in for 2
count = 2
>>> in while 2
count = 0
>>> continue in while
>>> "Done."
*/
