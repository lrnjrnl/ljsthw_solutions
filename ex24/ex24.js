#!/usr/bin/env node
const readline = require('readline-sync');

class Game {
    constructor () {
        this.hp = Math.floor((Math.random() * 10) + 3);
    }

    say(prompt) {
        console.log(prompt);
    }

    die(message) {
        this.say(message);
        process.exit(1);
    }

    ask(prompt) {
        console.log(`[[You have ${this.hp} hit points.]]`);
        if(this.hp <= 0) {
            this.die("You died!");
        } else {
            return readline.question(prompt + ' ');
        }
    }

    addRoom(room) {
        this[room.name] = room;
        room.game = this;
    }

    play(name) {
        this[name].enter();
    }

    hit(amount) {
        this.hp -= amount;
    }
}

class Room {
    constructor (name) {
        this.name = name;
    }

    enter() {
        console.log("Implement me!");
    }
}

class Door extends Room {
    enter() {
        // they have to open the door to get the gold
        // what kind of puzzle will they solve?
        this.game.say("You hear a whining in the wind like a pleading voice, \"Call my " +
        "name.\" ");
        let next = this.game.ask("Say the name.");
        if(next === "Moonchild") {
            this.game.say("\"MOONCHILD!!!\"");
            this.game.say("The massive gate falls back to reveal a gleaming " + 
                "mountain of gold.");
            this.game.gold.enter();
        } else {
            this.game.say("\n\"Bastian, please! Save us!\"");
            this.game.door.enter();
        }
    }
}

class Spider extends Room {
    enter() {
        // they enter here, and the spider takes 10 hit points
        // if they live then they can run away
        this.game.say("Sparkling jewels fill your eyes with greed. Your fingers extend " +
            "when suddenly a shaggy tarantula sinks its venomous " + 
            "fangs into your hand.");
        this.game.hp -= 6;
        let next = this.game.ask("What do you do?");
        if(next === "run") {
            this.game.say("Clutching your bloodied hand, you rear back and let the lid " +
            "collapse shut.");
            this.game.rope.enter();
        } else {
            this.game.say("You can't do that here.");
            this.game.spider.enter();
        } 
    }
}

class Gold extends Room {
    enter() {
        // end of the game they win if they get the gold
        this.game.say("A dragon with umbrella-sized ears descends upon you.");
        this.game.say("Its claws shred through your tunic.");
        this.game.hp -= 2;
        let next = this.game.ask("What do you do?");
        if(next === "sing") {
            this.game.say("Lulled by your warbling rendintion of Puff Daddy's " +
            "\"I'll Be Missing You\", the dragon drifts off to sleep.");
            this.game.die("You collect the gold and go home.\nThe End");
        } else {
            this.game.say("You can't do that here.");
            this.game.gold.enter();
        }
    }
}

class Rope extends Room {
    enter() {
        // they are at the bottom of the well
        // they can go through the door to the gold
        // or go take a wrong turn to the spider
        this.game.say("You are shivering at the bottom of a well. To your left is a " +
            "revolving door.");
        this.game.say("At your feet is a gilded chest no taller than your knee.");

        let next = this.game.ask("What do you do?")
        if(next === "push") {
            this.game.say("You push through the revolving door.");
            this.game.say("The revolving door leads down a long narrow hall.");
            this.game.say("At the end of the hall, you reach a heavy iron gate that " +
                "stretches from floor to ceiling.");
            this.game.door.enter();
        } else if(next === "open") {
            this.game.say("You open the chest.");
            this.game.spider.enter();
        } else {
            this.game.say("You can't do that here.");
            this.game.rope.enter();
        }
    }
}

class Well extends Room {

    enter() {
        this.game.say("You are walking through the woods and see a well.");
        this.game.say("Walking up to it and looking down you see a shiny thing at the bottom.");
        let next = this.game.ask("What do you do?");

        if(next == "climb") {
            this.game.say("You climb down the rope.");
            this.game.rope.enter();
        } else if(next == "jump") {
            this.game.say("Yikes! Let's see if you survive!");
            this.game.hit(5);
            this.game.rope.enter();
        } else {
            this.game.say("You can't do that here.");
            this.game.well.enter();
        }
    }
}


let game = new Game();
game.addRoom(new Well("well"));
game.addRoom(new Rope("rope"));
game.addRoom(new Gold("gold"));
game.addRoom(new Spider("spider"));
game.addRoom(new Door("door"));
game.play("well");
