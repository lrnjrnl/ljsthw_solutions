#!/usr/bin/env node
// This is a test of how the "return" and "new" keywords can override one
// another in object instantiation.
// "return non-primitive object" overrides "new object"
// "new object" overrides "return primitive value"

function FactoryEgg () {
    this.eggType = '\"I was instantiated in a factory function!\"'
    this.sayType = function() {console.log(this.eggType)};
}

function ConstructorEgg (){
    this.eggType = '\"I was instantiated in a constructor function!\"'
    this.sayType = function() {console.log(this.eggType)};
}

// Chicken can be a factory function if supplied a "factory" argumnet,
//  but it can also be a constructor.
function Chicken (whichway) {
    if (whichway === "factory") {
        // This will override "new Chicken()" constructor
        return new FactoryEgg();
    } else {
        // this gets overridden with "new Chicken()" constructor
        return '\"I am a primitive return value!\"';
    }
}

// set the function's prototype for when it is invoked as a constructor
console.log("> Chicken.prototype = new ConstructorEgg();");
Chicken.prototype = new ConstructorEgg();

console.log("\nThe following chicken eggs come from the same " +
    "\nChicken function to demonstrate how keywords " +
    "\n'new' and 'return' can override one another.");
console.log("\n>>> RETURN OBJECT overrides NEW OBJECT");

console.log("> chelsea = Chicken('factory');");
chelsea = Chicken('factory');
console.log("> console.log(chelsea);");
console.log(chelsea);
console.log("> chelsea.sayType();");
chelsea.sayType();

console.log("\n>>> RETURN PRIMITIVE VALUE");

console.log("> charlotte = Chicken('return primitive without new operator!');");
charlotte = Chicken('return primitive without new operator!');
console.log("> console.log(charlotte)");
console.log(charlotte)

console.log("\n>>> NEW OBJECT overrides RETURN PRIMITIVE VALUE");
console.log("> charlene = new Chicken('new overrides return primitive!');")
charlene = new Chicken('new overrides return primitive!');
console.log("> console.log(charlene);");
console.log(charlene);
console.log("> charlene.sayType();");
charlene.sayType();
